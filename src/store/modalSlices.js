import { createSlice } from "@reduxjs/toolkit";

const ModalSlices = createSlice({
  name: "modals",
  initialState: {
    isModalProduct: false,
    isModalDelCart: false,
    isModalDelFavorite: false,
    isModalForm: false,
  },
  reducers: {
    setIsModalProduct(state, action) {
      state.isModalProduct = action.payload;
    },
    setIsModalDelCart(state, action) {
      state.isModalDelCart = action.payload;
    },
    setIsModalDelFavorite(state, { payload }) {
      state.isModalDelFavorite = payload;
    },
    setModalForm(state, { payload }) {
      state.isModalForm = payload;
    },
    toggelModal(state, { payload }) {
      const { modalName } = payload;
      state[modalName] = !state[modalName];
    },
  },
});

export default ModalSlices.reducer;
export const { toggelModal } = ModalSlices.actions;
