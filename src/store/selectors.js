export const selectorError = (state) => state.root.error;
export const selectorProducts = (state) => state.root.products;
export const selectorCurrentProduct = (state) => state.root.currentProduct;
export const selectorCarts = (state) => state.root.carts;
export const selectorFavorites = (state) => state.root.favorites;

export const selectorIsModalProduct = (state) => state.modals.isModalProduct;
export const selectorIsModalDelCart = (state) => state.modals.isModalDelCart;
export const selectorIsModalDelFavorite = (state) =>
  state.modals.isModalDelFavorite;
export const selectorIsModalForm = (state) => state.modals.isModalForm;
