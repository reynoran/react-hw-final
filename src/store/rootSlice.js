import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import sendRequest from "../helpers/sendRequest";

export const actionFetchAsyncThunk = createAsyncThunk(
  "product/fetchProduct",
  async () => {
    const savedProducts = JSON.parse(localStorage.getItem("products"));
    if (savedProducts) {
      return savedProducts;
    } else {
      try {
        const data = await sendRequest("products.json");
        return data.map((product) => ({
          ...product,
          addedToCart: false,
          addedToFavorite: false,
          basketCounter: 0,
        }));
      } catch (error) {
        console.error(
          "Something in FetchAsyncThunk had gone wrong. ERROOORR!!",
          error
        );
        throw error;
      }
    }
  }
);

const initialState = {
  error: null,
  products: [],
  currentProduct: {},
  carts: JSON.parse(localStorage.getItem("carts")) || [],
  favorites: JSON.parse(localStorage.getItem("favorites")) || [],
};

const rootSlice = createSlice({
  name: "root",
  initialState,
  reducers: {
    setProducts(state, action) {
      state.products = action.payload;
    },
    setCurrentProduct(state, action) {
      state.currentProduct = action.payload;
    },
    setToCart(state, { payload }) {
      state.carts = payload;
    },
    setFavorite(state, { payload }) {
      state.favorites = payload;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(actionFetchAsyncThunk.fulfilled, (state, { payload }) => {
        state.products = payload;
      })
      .addCase(actionFetchAsyncThunk.rejected, (state, action) => {
        state.error = action.error.message;
      });
  },
});

export default rootSlice.reducer;
export const { setProducts, setCurrentProduct, setToCart, setFavorite } =
  rootSlice.actions;
