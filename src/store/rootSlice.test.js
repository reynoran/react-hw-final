import rootReducer, {
  setProducts,
  setCurrentProduct,
  setToCart,
  setFavorite,
  actionFetchAsyncThunk,
} from "./rootSlice";

const initialState = {
  error: null,
  products: [],
  currentProduct: {},
  carts: [],
  favorites: [],
};

describe("rootSlice reducers tests", () => {
  test("should handle initial state when if's 'undefined' and action is 'empty'", () => {
    expect(rootReducer(undefined, {})).toEqual(initialState);
  });

  test("should handle reducer setProducts", () => {
    const newProduct = [
      { id: 1, name: "Product 1" },
      { id: 2, name: "Product 2" },
    ];
    const expectedState = { ...initialState, products: newProduct };

    expect(rootReducer(initialState, setProducts(newProduct))).toEqual(
      expectedState
    );
  });

  test("should handle reducer setProducts", () => {
    const newProduct = [
      { id: 1, name: "Product 1" },
      { id: 2, name: "Product 2" },
    ];
    const expectedState = { ...initialState, products: newProduct };

    expect(rootReducer(initialState, setProducts(newProduct))).toEqual(
      expectedState
    );
  });

  test("should handle reducer setCurrentProduct", () => {
    const currentProduct = { id: 1, name: "Product 1" };

    expect(
      rootReducer(initialState, setCurrentProduct(currentProduct))
    ).toEqual({ ...initialState, currentProduct });
  });

  test("should handle reducer setToCart", () => {
    const carts = [{ id: 1, name: "Product 1", quantity: 1 }];

    expect(rootReducer(initialState, setToCart(carts))).toEqual({
      ...initialState,
      carts,
    });
  });

  test("should handle reducer setFavorite", () => {
    const favorites = [{ id: 1, name: "Product 1" }];

    expect(rootReducer(initialState, setFavorite(favorites))).toEqual({
      ...initialState,
      favorites,
    });
  });
});

describe("rootSlice extraReducers tests", () => {
  test("should actionFetchAsyncThunk on fulfilled", () => {
    const products = [{ id: 1, name: "Product 1" }];
    const action = {
      type: actionFetchAsyncThunk.fulfilled.type,
      payload: products,
    };
    const newState = rootReducer(initialState, action);

    expect(newState).toEqual({
      ...initialState,
      products,
    });
  });
});
