export const handleSubmit = (values) => {
  const {
    firstname,
    lastname,
    age,
    company,
    postcode,
    country,
    city,
    state,
    address,
    apartment,
    phone,
  } = values;

  console.log(`
You have entered the following information about yourself:
        
    • Your name: ${lastname} ${firstname};
    • Age: ${age} years old;
    • You work in the "${company}";
    • You have adress: ${postcode}, ${country}, ${city} city, ${state} region, ${apartment}, ${address} street;
    • Phone number: ${phone};
    `);
};
