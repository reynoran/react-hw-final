import * as yup from "yup";

const requiredText = "The field is required";
const nameValidation = yup
  .string()
  .required(requiredText)
  .min(3, "The name is too short (min 3 letters)")
  .max(21, "The name is too long (max 21 letters)")
  .matches(/[a-zA-Z]/, "Only Latin letters allowed");

const phoneValidation = yup
  .string()
  .required("The field is required")
  .matches(
    /^\+?([0-9]{1,4})?([-.\s\(\)]*[0-9]){3,19}([-.\s\(\)]*[0-9]{1,4})?$/,
    "Invalid phone number format"
  );
// const phoneValidation = yup
//   .string()
//   .required("The field is required")
//   .transform((value) => value.replace(/[^\d]/g, ""))
//   .matches(/^\d+$/, "Only numbers");

export const validation = yup.object({
  firstname: nameValidation,
  lastname: nameValidation,
  country: nameValidation,
  state: nameValidation,
  city: nameValidation,
  address: nameValidation,
  age: yup
    .number()
    .typeError("Only numbers")
    .required(requiredText)
    .min(12, "You must be over 12 years old")
    .max(120, "Too long! So many do not live"),
  // postcode: yup
  //   .number()
  //   .typeError("Only numbers")
  //   .required(requiredText)
  //   .test(
  //     "len",
  //     "Too short! At least six numbers",
  //     (value) => value && value.toString().length >= 6
  //   ),
  postcode: yup // Саме така валідація необхідна, тому що "0" на початку числового ряду у почтовому індексі не враховуються (наприклад, код: 010010, буде зчитуватися як 10010), тому загальна довжина числового ряду стає значно менше, та валідація не проходить
    .string()
    .required(requiredText)
    .matches(/^\d{6,}$/, "Too short! At least six numbers"),
  // phone: yup.number().typeError("Only numbers").required(requiredText),
  phone: phoneValidation,
});
