import React from "react";
import PropTypes from "prop-types";
import { ErrorMessage, useField } from "formik";
import cn from "classnames";
import { InputMask } from "@react-input/mask";

import "./Input.scss";

const InputMaskBox = (props) => {
  const {
    className,
    type = "text",
    placeholder,
    name,
    label,
    error,
    ...restProps
  } = props;

  const [field] = useField(name);

  return (
    <label className={cn("label__order", className)}>
      <p className="label__title">{label}</p>
      <InputMask
        className="input__order"
        type={type}
        placeholder={placeholder}
        name={name}
        {...field}
        value={field.value || ""}
        {...restProps}
      />
      <ErrorMessage name={name} className={"error-message"} component={"p"} />
    </label>
  );
};

InputMaskBox.propTypes = {
  className: PropTypes.string,
  type: PropTypes.string,
  placeholder: PropTypes.string,
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  error: PropTypes.bool,
  restProps: PropTypes.object,
};

export default InputMaskBox;
