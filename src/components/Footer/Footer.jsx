import React from "react";
import PropTypes from "prop-types";
import cn from "classnames";

import "./Footer.scss";

const Footer = ({ className, children }) => {
  return (
    <footer className={cn("inner", "footer", className)}>
      <p>React HomeWork | Kucher Andrii | 2024</p>
      {children && <p>{children}</p>}
    </footer>
  );
};

Footer.propTypes = {
  className: PropTypes.string,
  children: PropTypes.any,
};

export default Footer;
