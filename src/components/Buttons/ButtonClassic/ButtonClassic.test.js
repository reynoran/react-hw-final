import React from "react";
import { render, screen } from "@testing-library/react";
import ButtonClassic from "./ButtonClassic";

describe("ButtonClassic tests", () => {
  test("should render chilren text", () => {
    render(<ButtonClassic>test</ButtonClassic>);
    expect(screen.getByText(/test/i)).toBeInTheDocument();
  });

  test("should apply passed className", () => {
    const { container } = render(
      <ButtonClassic className="test-class">test</ButtonClassic>
    );
    expect(container.firstChild).toHaveClass("test-class");
  });

  test.each([["button"], ["submit"]])(
    "should set button type to '%s'",
    (type) => {
      const { container } = render(
        <ButtonClassic type={type}>test</ButtonClassic>
      );
      expect(container.firstChild).toHaveAttribute("type", type);
    }
  );

  test("should default button type to 'button' if not specified", () => {
    const { container } = render(<ButtonClassic>test</ButtonClassic>);
    expect(container.firstChild).toHaveAttribute("type", "button");
  });

  test("should pass other props to button element", () => {
    const { container } = render(
      <ButtonClassic data-test="data-attr">test</ButtonClassic>
    );
    expect(container.firstChild).toHaveAttribute("data-test", "data-attr");
  });

  test("should call onClick handler", () => {
    const handleClick = jest.fn();
    render(<ButtonClassic onClick={handleClick}>test</ButtonClassic>);
    screen.getByText(/test/i).click();
    expect(handleClick).toHaveBeenCalledTimes(1);
  });
});
