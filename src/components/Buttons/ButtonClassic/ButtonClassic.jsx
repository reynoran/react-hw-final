import React from "react";
import PropTypes from "prop-types";
import cn from "classnames";

import "./ButtonClassic.scss";

const ButtonClassic = (props) => {
  const { className, type = "button", onClick, children, ...restProps } = props;

  return (
    <button
      className={cn("btn", className)}
      type={type}
      onClick={onClick}
      {...restProps}
    >
      {children}
    </button>
  );
};

ButtonClassic.propTypes = {
  children: PropTypes.any,
  className: PropTypes.string,
  type: PropTypes.string,
  onClick: PropTypes.func,
  restProps: PropTypes.object,
};

export default ButtonClassic;
