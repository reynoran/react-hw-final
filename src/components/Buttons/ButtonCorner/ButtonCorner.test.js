import React from "react";
import { render, screen } from "@testing-library/react";
import ButtonCorner from "./ButtonCorner";

describe("ButtonCorner tests", () => {
  test("should render chilren text", () => {
    render(<ButtonCorner>test</ButtonCorner>);
    expect(screen.getByText(/test/i)).toBeInTheDocument();
  });

  test("should apply passed className", () => {
    const { container } = render(
      <ButtonCorner className="test-class">test</ButtonCorner>
    );
    expect(container.firstChild).toHaveClass("test-class");
  });

  test("should set button type to 'button'", () => {
    const { container } = render(
      <ButtonCorner type="button">test</ButtonCorner>
    );
    expect(container.firstChild).toHaveAttribute("type", "button");
  });

  test("should default button type to 'button' if not specified", () => {
    const { container } = render(<ButtonCorner>test</ButtonCorner>);
    expect(container.firstChild).toHaveAttribute("type", "button");
  });

  test("should pass other props to button element", () => {
    const { container } = render(
      <ButtonCorner data-test="data-attr">test</ButtonCorner>
    );
    expect(container.firstChild).toHaveAttribute("data-test", "data-attr");
  });

  test("should call onClick handler", () => {
    const handleClick = jest.fn();
    render(<ButtonCorner onClick={handleClick}>test</ButtonCorner>);
    screen.getByText(/test/i).click();
    expect(handleClick).toHaveBeenCalledTimes(1);
  });
});
