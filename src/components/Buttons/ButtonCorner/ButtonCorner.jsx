import React from "react";
import PropTypes from "prop-types";
import cn from "classnames";

import "./ButtonCorner.scss";

const ButtonCorner = (props) => {
  const { className, type = "button", onClick, children, ...restProps } = props;
  return (
    <button
      className={cn("btn__corner", className)}
      type={type}
      onClick={onClick}
      {...restProps}
    >
      {children}
    </button>
  );
};

ButtonCorner.propTypes = {
  className: PropTypes.string,
  type: PropTypes.string,
  onClick: PropTypes.func,
  children: PropTypes.any,
  restProps: PropTypes.object,
};

export default ButtonCorner;
