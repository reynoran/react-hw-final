import React from "react";
import PropTypes from "prop-types";

import ButtonCorner from "./ButtonCorner";
import CloseIcon from "../icons/close.svg?react";
// import { ReactComponent as CloseIcon } from "../icons/close.svg";

const ButtonCornerClose = ({ onClick }) => {
  return (
    <ButtonCorner
      className="btn__corner-right test-button_close"
      onClick={onClick}
    >
      <CloseIcon className="svg svg--close" />
    </ButtonCorner>
  );
};

ButtonCornerClose.propTypes = {
  onClick: PropTypes.func,
};

export default ButtonCornerClose;
