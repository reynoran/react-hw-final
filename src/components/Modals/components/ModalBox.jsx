import React from "react";
import PropTypes from "prop-types";
import cn from "classnames";
import "./ModalBase.scss";

const ModalBox = ({ children, className }) => {
  return <div className={cn("modal", className)}>{children}</div>;
};

ModalBox.propTypes = {
  children: PropTypes.any,
  className: PropTypes.string,
};

export default ModalBox;
