import React from "react";
import PropTypes from "prop-types";

import "./ModalBase.scss";

const ModalMain = ({ children, currentProduct }) => {
  const { price, color, vendorCode } = currentProduct;
  return (
    <div className="modal__main">
      <p className="modal__main-title">{children}</p>
      <div className="modal__main-text">
        <p>Price: ${price}</p>
        <p>Color: {color}</p>
        <p>Article: {vendorCode}</p>
      </div>
    </div>
  );
};

ModalMain.propTypes = {
  children: PropTypes.any,
  currentProduct: PropTypes.object,
};

export default ModalMain;
