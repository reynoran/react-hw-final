import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import ModalAddProduct from "./ModalAddProduct";

describe("ModalAddProduct tests", () => {
  const mockCurrentProduct = {
    name: "Test Product",
    path: "/path/to/image.jpg",
    price: 100,
    color: "red",
    vendorCode: "123ABC",
  };

  const onClose = jest.fn();
  const onAddToCart = jest.fn();

  //   beforEach(() => {
  //     render(
  //       <ModalAddProduct
  //         isOpen={true}
  //         onClose={onClose}
  //         currentProduct={mockCurrentProduct}
  //         onAddToCart={onAddToCart}
  //       />
  //     );
  //   });

  const renderModal = () => {
    render(
      <ModalAddProduct
        isOpen={true}
        onClose={onClose}
        currentProduct={mockCurrentProduct}
        onAddToCart={onAddToCart}
      />
    );
  };

  test("should render modal with product details", () => {
    renderModal();
    // const { container } = render(
    //   <ModalAddProduct
    //     isOpen={true}
    //     onClose={onClose}
    //     currentProduct={mockCurrentProduct}
    //     onAddToCart={onAddToCart}
    //   />
    // );

    // console.log(container.innerHTML);

    // expect(screen.getByText(/Add Test Product to cart /i)).toBeInTheDocument();
    expect(screen.getByText(/Test Product/i)).toBeInTheDocument();
    expect(screen.getByText(/Price: \$100/i)).toBeInTheDocument();
    expect(screen.getByText(/Color: red/i)).toBeInTheDocument();
    expect(screen.getByText(/Article: 123ABC/i)).toBeInTheDocument();
    expect(screen.getByText("ADD TO CART")).toBeInTheDocument();

    const img = screen.getByAltText(/Test Product/i);
    expect(img).toHaveAttribute("src", "/path/to/image.jpg");
  });

  test("should call onClick when clicking outside the modal", () => {
    // renderModal();
    const { container } = render(
      <ModalAddProduct
        isOpen={true}
        onClose={onClose}
        currentProduct={mockCurrentProduct}
        onAddToCart={onAddToCart}
      />
    );

    const modalWrapper = container.querySelector(".madal__wrapper");
    expect(modalWrapper).toBeInTheDocument();

    fireEvent.click(modalWrapper);
    expect(onClose).toHaveBeenCalledTimes(1);
  });

  test("should call onClose when clicking the close button", () => {
    const onCloseBtn = jest.fn();
    const { container } = render(
      <ModalAddProduct
        isOpen={true}
        onClose={onCloseBtn}
        currentProduct={mockCurrentProduct}
        onAddToCart={onAddToCart}
      />
    );

    fireEvent.click(container.querySelector(".test-button_close"));
    expect(onClose).toHaveBeenCalledTimes(1);
  });

  test("should render modal when isOpen is true", () => {
    renderModal();
    expect(screen.getByText(/Test Product/i)).toBeInTheDocument();
  });

  test("should not render modal when isOpen is false", () => {
    render(
      <ModalAddProduct
        isOpen={false}
        onClose={onClose}
        currentProduct={mockCurrentProduct}
        onAddToCart={onAddToCart}
      />
    );
    expect(screen.queryByText(/Test Product/i)).not.toBeInTheDocument();
  });

  test("should call onAddToCart and onClose when 'ADD TO CART' button is clicked", () => {
    renderModal();
    // render(
    //   <ModalAddProduct
    //     isOpen={true}
    //     onClose={onClose}
    //     currentProduct={mockCurrentProduct}
    //     onAddToCart={onAddToCart}
    //   />
    // );
    fireEvent.click(screen.getByText("ADD TO CART"));

    console.log(
      "Log the number of times onClose was called:",
      onClose.mock.calls.length
    );

    expect(onClose).toHaveBeenCalledTimes(2);
    expect(onAddToCart).toHaveBeenCalledTimes(1);
  });
});
