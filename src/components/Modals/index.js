import ModalAddProduct from "./ModalAddProduct";
import ModalDelCart from "./ModalDelCart";
import ModalDelFavorite from "./ModalDelFavorite";
import ModalFormToOrder from "./ModalFormToOrder";

export { ModalAddProduct, ModalDelCart, ModalDelFavorite, ModalFormToOrder };
