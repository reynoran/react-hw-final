import React from "react";
import PropTypes from "prop-types";

import Pages, { CardsWrap } from "./components";
import { CardFavorite } from "../components/Cards";

const FavoritePage = (props) => {
  const { onCurrentProduct, isOpenModalPruduct, handleModalDelFavorite } =
    props;

  const localFavorites = JSON.parse(localStorage.getItem("favorites")) || [];
  
  return (
    <Pages>
      <CardsWrap>
        {localFavorites.map((product) => {
          return (
            <CardFavorite
              key={product.vendorCode}
              product={product}
              onModalProduct={() => {
                isOpenModalPruduct();
                onCurrentProduct(product);
              }}
              onModalDelFavorite={() => {
                handleModalDelFavorite();
                onCurrentProduct(product);
              }}
            />
          );
        })}
      </CardsWrap>
    </Pages>
  );
};

FavoritePage.propTypes = {
  onCurrentProduct: PropTypes.func,
  isOpenModalPruduct: PropTypes.func,
  handleModalDelFavorite: PropTypes.func,
};

export default FavoritePage;
