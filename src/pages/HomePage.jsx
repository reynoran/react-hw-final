import React from "react";
import PropTypes from "prop-types";

import Pages, { CardsWrap } from "./components";
import { CardGeneral } from "../components/Cards";

const HomePage = (props) => {
  const {
    products,
    handleModalProduct,
    onCurrentProduct,
    handleFavorite,
  } = props;
  

  return (
    <Pages>
      <CardsWrap>
        {products.map((product) => {
          return (
            <CardGeneral
              key={product.vendorCode}
              product={product}
              onModalProduct={() => {
                handleModalProduct();
                onCurrentProduct(product);
              }}
              handleFavorite={handleFavorite}
            />
          );
        })}
      </CardsWrap>
    </Pages>
  );
};

HomePage.propTypes = {
  products: PropTypes.array,
  handleModalProduct: PropTypes.func,
  onCurrentProduct: PropTypes.func,
  handleFavorite: PropTypes.func,
};

export default HomePage;
