import { createContext, useState } from "react";

export const StoreContext = createContext({});

const ContextProvider = ({ children }) => {
  const [isGridSwitcher, setIsGridSwitcher] = useState(true);

  const showGridCart = () => {
    setIsGridSwitcher(true);
  };
  
  const showGridLine = () => {
    setIsGridSwitcher(false);
  };

  return (
    <StoreContext.Provider
      value={{
        isGridSwitcher,
        showGridCart,
        showGridLine
      }}
    >
      {children}
    </StoreContext.Provider>
  );
};

export default ContextProvider;
